<?php

use Illuminate\Support\Facades\Route;
use Simplexi\Greetr\Greetr;

Route::get('/greet/{name}', function($sName) {
    $oGreetr = new Greetr(); 
    return $oGreetr->greet($sName);
});