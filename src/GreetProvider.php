<?php

namespace App\packages\simplexi\greetr\src;

use Illuminate\Support\ServiceProvider;

class GreetProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/web.php');
        $this->loadMigrationsFrom(__DIR__.'database/migrations');    
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
